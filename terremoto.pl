%Aqui se agregan todos los edificios antisismicos a una lista y se
% retorna

agregarEdifA(AS):-
	findall([X,Y], (casaA(X,Y); edificioA(X,Y)), AS).

% Aqui se realiza la simulacion del catastro, donde se obtiene un
% porcentaje utilizando la lista de edificios antisismicos y la lista de
% edificaciones totales.
terremoto:-
	agregarEdif(E),
	casasNormales(N),
	append(E,N,L),
	agregarEdifA(AS),
	length(L,NEdificaciones),
	length(AS,NASProtegidas),
	Percent is 100*NASProtegidas/NEdificaciones,
	(
	 Percent > 70, write('Apto');
	 write('No apto')
	),
	round(Percent,R),
	writeInforme('informe_terremoto.out', R), !.
