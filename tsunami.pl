% Es true si la coordenada [X,Y] esta protegida de olas.
protegidoDeOlas(X,Y):-
	dimension(L,_),
	X < L, C is X+1,
	(
	 edificioA(C,Y);
	 rascacielo(C,Y);
	 faldaCerro(C,Y);
	 protegidoDeOlas(C,Y)
	),!.

tsunami:-
	agregarEdif(E),
	casasNormales(N),
	append(E,N,L),
	findall([X,Y], 
	(
	 member([X,Y], L),
	 protegidoDeOlas(X,Y) 
	), P),

	length(L,NEdificaciones),
	length(P,NProtegidas),

	Percent is 100*NProtegidas/NEdificaciones,
	(
	 Percent > 70, write('Apto');
	 write('No apto')
	),
	round(Percent,R),
	writeInforme('informe_tsunami.out', R), !.  
