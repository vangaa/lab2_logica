%Retorna True si encuentra algun error en cualquier rio, returna false si 
%todos los rios estan bien.
error5:-
     rio(Xa,Ya,Xb,Yb,Xc,Yc),
     (
      not((abs(Ya,Yc), abs(Xb,Xc));(abs(Xa,Xc), abs(Yb,Yc))),
      not((Xa == Xb, [Xc,Yc] == [-1,-1])),
      not((Ya == Yb, [Xc,Yc] == [-1,-1]))
     ),
     writeError('informeError.out','Error5. El centro de algun rio no corresponde a uno valido.')
     , !.
