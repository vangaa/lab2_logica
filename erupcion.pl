% Lista todas las coordenadas de 'Edif' que estan protegidas de una erupcion y 
% las coloca en P.
%
protegidosDeErupcion(Edif,P):-
	findall([X,Y],
	(
	 member([X,Y],Edif),
	 (
	  protegidoPorRio(X,Y);
	  faldaCerro(X-1,Y)
	 )
	),P).

erupcion:-
	agregarEdif(E),
	casasNormales(N),
	append(E,N,L),
	protegidosDeErupcion(L,P),
	length(L,NEdificaciones),
	length(P,NProtegidas),
	Percent is 100*NProtegidas/NEdificaciones,
	(
	 Percent > 70, write('Apto');
	 write('No apto')
	),
	round(Percent,R),
	writeInforme('informe_erupcion.out', R), !.
