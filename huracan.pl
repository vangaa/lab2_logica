% true si la coordenada [X,Y] es protegida por algun cerro en caso de huracan 
% en direccion Dir.
% @param Dir Direccion del huracan. Debe ser la letra inicial de algunas de 
% las direcciones: Sur, Norte, Este u Oeste.
%
protegidoPorCerro(X,Y,Dir):-
    member(Dir,['n','N']), X > 1, C is X-1,
    (
     faldaCerro(C,Y);
     protegidoPorCerro(C,Y,Dir)
    ), !;
    
    member(Dir,['o','O']), Y > 1, C is Y-1,
    (
     faldaCerro(X,C);
     protegidoPorCerro(X,C,Dir)
    ), !;

    member(Dir,['e','E']), dimension(_,Ly),
    Y < Ly-2, C is Y+1,
    (
     faldaCerro(X,C);
     protegidoPorCerro(X,C,Dir)
    ), !;

    member(Dir,['s','S']), dimension(Lx,_),
    X < Lx-2, C is X+1,
    (
     faldaCerro(C,Y);
     protegidoPorCerro(C,Y,Dir)
    ), !.

% true si la coordenada (X,Y) es protegida por algun edificio en caso de 
% huracan en direccion Dir.
% @param Dir Es la direccion del huracan
%
protegidoPorEdificio(X,Y,Dir):-
    (
     member(Dir,['n','N']), Xn is X-1, Yn is Y;
     member(Dir,['s','S']), Xn is X+1, Yn is Y;
     member(Dir,['e','E']), Xn is X, Yn is Y+1;
     member(Dir,['o','O']), Xn is X, Yn is Y-1
    ),
    (
     edificioA(Xn,Yn); rascacielo(Xn,Yn)
    ), !.

huracan(Dir):-
    agregarEdif(E), casasNormales(N), append(E,N,T),
    findall(
    [X,Y],
    (
     member([X,Y],T),
     (
      protegidoPorCerro(X,Y,Dir);
      protegidoPorEdificio(X,Y,Dir)
     )
    ), P), 

    length(T,NEdificaciones),
    length(P,NProtegidas),

    Percent is 100*NProtegidas/NEdificaciones,
    (
     Percent > 70, write('Apto');
     write('No apto')
    ),
    round(Percent,R),
    writeInforme('informe_huracan.out', R), !.
