
error4(Edificaciones):-
	(
	 error4p1(Edificaciones);
	 error4p2;
	 error4p3;
	 error4p4(Edificaciones);
	 error4p5
	),!.

error4p1(Edif):-
	length(Edif, ELength),
	eliminarRepetidos(Edif,L),
	length(L,Largo),
	ELength > Largo,
	writeError('informeError.out','Error4.1. Hay edificaciones superpuestas')
	,!.

error4p2:-
	findall(
	[X,Y],
	(
	 rio(X,Y,_,_,_,_);
	 rio(_,_,X,Y,_,_)
	), C),
	eliminarRepetidos(C,U),
	length(C,LargoC), length(U,LargoU),
	LargoC =\= LargoU,
	writeError('informeError.out','Error4.2. Hay dos rios que comienzan o terminan en el mismo lugar')
	, !.

error4p3:-
	findall([X,Y], cerro(X,Y), L),
	cerro(X1,Y1),
	aparece([X1,Y1],L,Cuantas),
	Cuantas > 1,
	writeError('informeError.out','Error4.3. Hay dos cerros definidos en el mismo lugar')
	, !.

error4p4(E):-
	member([X,Y],E),
	(
	 esRio(X,Y) ->
	 writeError('informeError.out','Error4.4. Hay edificaciones sobre algun rio')
	 ;
	 faldaCerro(X,Y) ->
	 writeError('informeError.out','Error4.4. Hay edificaciones sobre un cerro')
	), !.

error4p5:-
	cerro(Xc,Yc),
	(
	 esRio(Xc,Yc);

	 esRio(Xc+1,Yc-1); esRio(Xc+1,Yc); esRio(Xc+1,Yc+1);
	 esRio(Xc-1,Yc-1); esRio(Xc-1,Yc); esRio(Xc-1,Yc+1);

	 esRio(Xc,Yc-1); esRio(Xc,Yc+1)
	),
	writeError('informeError.out','Error4.5. Hay algun rio sobre un cerro')
	,!.

