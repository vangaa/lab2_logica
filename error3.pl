
% Es true, si en algun rio, ninguna coordenada se encuentra al borde del
% plano.
%
error3:-
	dimension(Xd, Yd),
	rio(Xi,Yi,Xf,Yf,_,_),
	Xi =\= 0, Yi =\= 0,
	Xf =\= 0, Yf =\= 0,
	Xi =\= Xd-1, Yi =\= Yd-1,
	Xf =\= Xd-1, Yf =\= Yd-1,
	writeError('informeError.out','Error3. Algun rio no esta definido por a lo menos un borde del plano')
	,!.
