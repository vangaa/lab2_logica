# Laboratorio 2 Lógica

---

## Resumen

Existe una ciudad llamada desastrópolis la cual es azotada constantemente por
cuatro catástrofes diferentes:

 - Tsunami
 - Terremoto
 - Huracán
 - Erupción volcánica

Se ha contratado a un agente inteligente el cual debe discernir si la ciudad
es apta o no, dada cualquier catástrofe. La ciudad es apta si mas del 70% de
las viviendas sobrevive la catástrofe. Para poder entregar una respuesta acorde
a la situación, el agente tiene un plano de la ciudad el cual tiene la ubicación
de las viviendas y también de los ríos y cerros los cuales sirven para palear el
daño.

Antes de poder decir si la ciudad es apta o no, debe primero *checkear* que el
plano sea coherente, es decir, que no tenga errores. Si es que los tiene, se
debe informar el error.


## Ejecución

### Archivo info.pl

En la carpeta principal tiene que existir un archivo con el nombre *info.pl* el cual trae
las coordenadas de los edificios especiales, de los ríos y de los cerros. Un ejemplo puede
ser:

    dimension(8,8).
    rio(3,0,7,3,3,3).
    cerro(3,6).
    rascacielo(0,7).
    rascacielo(1,5).
    rascacielo(3,4).
    edificioA(1,2).
    edificioA(5,1).
    edificioA(6,5).
    casaA(2,1).
    casaA(5,7).
    casaA(6,0).
    casaA(6,1).
    casaA(6,2).

Se debe definir a lo menos una instancia de cada tipo.

### Distribuciones GNU/Linux

Debe tener instalado algún intérprete de Prolog, de preferencia [SWI-Prolog][swi-pl page], y ejecutar
en la terminal

    swipl main.pl

Una vez en ejecución usted puede hacer las consultas `tsunami.`, `terremoto`. , `huracan.` o
`erupcion.` las cuales dirán por pantalla si la ciudad es apta o no luego de la catástrofe
y escribirán en un archivo de salida el valor porcentual de viviendas que la sobreviven.

Estas catástrofes no son acumulativas, vale decir, cada una es independiente de la otra y su
ejecución no toma en cuenta el resultado de la anterior.

La catástrofe *huracan* debe recibir una dirección como parámetro. Estas, son la inicial de
algún punto cardinal.

Ejm:

    tsunami(s).	% Simula un huracán con viento dirección sur

[swi-pl page]: http://www.swi-prolog.org/