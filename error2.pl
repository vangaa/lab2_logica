% Es true si hay construcciones, rios o cerros que estan definidos fuera del
% plano de la ciudad.
%
error2:-
	dimension(Xd, Yd),
	(
	error2casaA(Xd,Yd);
	error2edificioA(Xd,Yd);
	error2rascacielo(Xd,Yd);
	error2rio(Xd,Yd);
	error2cerro(Xd,Yd)
	),
	writeError('informeError.out','Error2. Hay declaraciones fuera del plano') 
	,!.

error2casaA(Xd,Yd):-
	casaA(Xca, Yca),
	(
	Xca < 0; Yca < 0; 
	Xca >= Xd; Yca >= Yd
	), !.

error2edificioA(Xd,Yd):-
	edificioA(Xed, Yed),
	(
	Xed < 0; Yed < 0; 
	Xed >= Xd; Yed >= Yd
	), !.

error2rascacielo(Xd,Yd):-
	rascacielo(Xrc, Yrc),
	(
	Xrc < 0; Yrc < 0; 
	Xrc >= Xd; Yrc >= Yd
	), !.

error2rio(Xd,Yd):-
	rio(Xi,Yi,Xf,Yf,_,_),
	(
	Xi < 0; Xf < 0;
	Yi < 0; Yf < 0;
	Xi >= Xd; Yi >= Yd;
	Xf >= Xd; Yf >= Yd 
	), !.
	
error2cerro(Xd,Yd):-
	cerro(Xcer, Ycer),
	(
	Xcer < 0; Ycer < 0; 
	Xcer >= Xd; Ycer >= Yd
	), !.


