
% Genera una lista de numeros de mayor a menor desde 'Limite' hasta 0.
% @param Limite Es el primer y mayor numero de la lista
% @param L Es la lista decreciente
%
listaDecreciente(0,[0]).
listaDecreciente(Limite,L):-
	Lp is Limite-1, listaDecreciente(Lp,S), L = [Limite|S], !.

% Busca las casas normales (aquellas coordenadas donde no hay nada definido) y
% las coloca en la lista CN.
%
casasNormales(CN):-
    dimension(Xd,Yd),
    Lx is Xd-1, Ly is Yd-1,
    listaDecreciente(Lx,Cols),
    listaDecreciente(Ly,Filas),
    findall(
    [X,Y],
    (
     member(X,Cols), member(Y,Filas),
     not(casaA(X,Y)),
     not(edificioA(X,Y)),
     not(rascacielo(X,Y)),
     not(esRio(X,Y)),
     not(faldaCerro(X,Y))
    ), CN), !.

% true si X aparece N veces en la lista [H|T]
aparece(_,[],0).
aparece(X,[H|T],N):-
	X == H, aparece(X,T,M), N is M+1, !;
	aparece(X,T,L), N is L.

% Lista todos los edificios especiales: casaA, edificioA y rascacielo.
% @param L Es la lista de edificios.
agregarEdif(L):-
	findall([X,Y], (casaA(X,Y); edificioA(X,Y);rascacielo(X,Y)), L).

% Elimina los elementos repetidos de una lista y guarda los elementos no 
% repetidos en la lista L.
eliminarRepetidos([],[]).
eliminarRepetidos([H|T],L):-
	eliminarRepetidos(T,S), 
	(
	not(member(H,S)), L = [H|S]; 
	L = S
	), !.


% Es true si la coordenada (X,Y) corresponde a la falda de alguno de los 
% cerros. Se define falda del cerro como cualquier parte de un cerro.
% @param X Coordenada vertical
% @param Y Coordenada horizontal
%
faldaCerro(X,Y):-
	cerro(A,B),
	Dx is A-X, abs(Dx,Rx), Rx =< 1,
	Dy is B-Y, abs(Dy,Ry), Ry =< 1, !.

% Es true si la coordenada [X,Y] es parte de algun rio.
%
esRio(X,Y):-
	rio(Xi,Yi,Xf,Yf,Xc,Yc),
	(
	 X =:= Xc,
	 mayor(Yi,Yf,Ymayor),
	 menor(Yi,Yf,Ymenor),
	 Y =< Ymayor, Y >= Ymenor
	 ;
	 Y =:= Yc,
	 mayor(Xi,Xf,Xmayor),
	 menor(Xi,Xf,Xmenor),
	 X =< Xmayor, X >= Xmenor
	), !.

% Indica si M es el mayor entre A y B
mayor(A,B,M):-
	A > B -> M is A;
	M is B.

% Indica si M es el menor entre A y B
menor(A,B,M):-
	A < B -> M is A;
	M is B.

% Es true si la coordenada (X,Y) es protegida por algun rio en caso de 
% erupcion volcanica
% @param X Es la coordenada vertical
% @param Y Es la coordenada horizontal
protegidoPorRio(X,Y):-
	X >= 0,
	(
	esRio(X-1,Y);
	protegidoPorRio(X-1,Y)
	), !.

% Funcion para escribir el error detectado en el archivo de salida.
% @param FileName Es el nombre del fichero
% @param Text Es el texto que se escribe en el fichero
%
writeError(FileName, Text):-
	open(FileName,write,Stream),
	write(Stream,Text),
	nl(Stream),
	close(Stream).

% Escribe el resultado de la catastrofe en el archivo de salida.
% @param FileName Es el nombre del archivo de salida
% @param Percent Es el porcentaje de viviendas que quedaron en pie
%
writeInforme(FileName, Percent):-

	open(FileName,write,Stream),

	write(Stream,'Porcentaje de viviendas en pie: '),
	write(Stream,Percent),
	write(Stream,'%'),

	nl(Stream),
	close(Stream).

% Es true si existe algun error de cualquien tipo. Este error es reportado por
% pantalla y los detalles se escriben en un archivo de salida.
% @param Edif Es la lista de las edificaciones especiales (casaA, edificioA y
% rascacielo)
%
detectaErrores(Edif):-
	(
	 error1,nl, write(user_error, '======= Error 1.=======');
	 error2,nl, write(user_error, '======= Error 2. =======');
	 error3,nl, write(user_error, '======= Error 3. =======');
	 error4(Edif),nl, write(user_error, '======= Error 4. =======');
	 error5,nl, write(user_error, '======= Error 5. =======')
	),!.

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Programa principal. Se ejecuta al iniciar.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:-
	consult('error[12345].pl'), consult(info),
	consult(terremoto), consult(tsunami),
	consult(huracan), consult(erupcion),

	agregarEdif(E),
	(
	 not(detectaErrores(E)) -> write('=== Informacion sin errores. ===');
	 true
	),!.
